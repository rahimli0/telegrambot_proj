import json

import requests
import telebot
# Create your views here.
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery, InlineKeyboardMarkup

from panel.tasks import send_task_finished_info
from telegrambot_proj.settings import TELEGRAM_BOT_TOKEN

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

from telebot import types



def EmployeesButton(message,text):
    r = requests.get("{}".format('http://159.65.72.2/bot-api/employees/'))
    data = json.loads(r.text)
    key = ReplyKeyboardMarkup(True,True)
    # text = "Hello {}".format(message.from_user.first_name)
    print(data)
    for i in range(len(data['list'])):
        button = KeyboardButton(text=data['list'][i]['first_name'],request_contact=False)
        key.add(button)
    bot.send_message(message.from_user.id,text, reply_markup=key)
    pass

# @bot.message_handler(commands=['start'])
# def start(message):
#     text = "Hello {}".format(message.from_user.first_name)
#     bot.send_message(message.from_user.id,text)

@bot.message_handler(commands=['start'])
def start(message):
    try:
        r = requests.get("{}".format('http://159.65.72.2/bot-api/get-start/{}/{}/'.format(message.from_user.id,message.from_user.username,)),params={'first_name':message.from_user.first_name,'last_name':message.from_user.last_name,})
        data = json.loads(r.text)
        text = "{}, {}".format(message.from_user.username,data['message'])

    except:
        text = "{}".format(message.from_user.username)
    bot.send_message(message.from_user.id,text)


def EmployeesActiveTasksButton(message,text,username):
    r = requests.get("{}".format('http://159.65.72.2/bot-api/task-list/{}/'.format(username)))
    data = json.loads(r.text)
    keyboard = InlineKeyboardMarkup()
    # text = "Hello {}".format(message.from_user.first_name)
    print(data)
    buttons = []
    if len(data['list']) > 0:
        for i in range(len(data['list'])):
            # button = InlineKeyboardButton(text=data['list'][i]['id'],request_contact=False)
            # key.add(button)
            buttons.append(types.InlineKeyboardButton(text=str(data['list'][i]['title']), callback_data=str(data['list'][i]['it'])))
        keyboard.add(buttons)

        bot.send_message(message.chat.id, text, reply_markup=keyboard)
        return True
    else:
        bot.send_message(message.from_user.id, 'You have not tasks')
        return False
@bot.message_handler(commands=['finishtask'])
def finished_task_handler(message):
    cid = message.chat.id
    task_name = bot.send_message(cid, 'Select your task:')
    result = EmployeesActiveTasksButton(message,'Select your task:',message.from_user.username)
    if result:
        bot.register_next_step_handler(task_name , finished_task_callback)

def finished_task_callback(message):
    import telegram
    cid = message.chat.id
    try:
        id = int(message.text)
        r = requests.get("{}".format('http://159.65.72.2/bot-api/finished-task/{}/{}/'.format(id,message.from_user.username,)))
        data = json.loads(r.text)
        bot.send_message(cid, data['result'])
    except:
        bot.send_message(cid, 'Error has hapeneded, try again:')
        # bot.register_next_step_handler(task_name , finished_task_callback)



@bot.message_handler(commands=['deleteuser', 'edituser'])
def start(message):
    text = "Hello {}".format(message.from_user.username)
    bot.send_message(message.from_user.id,text)

@bot.message_handler(commands=['iamhere'])
def daily_check(message):
    try:
        r = requests.get('http://159.65.72.2/bot-api/daily-check/{}'.format(message.from_user.id))
        data = json.loads(r.text)
        text = "Dear {}, {}".format(message.from_user.first_name, data['result'])
    except:
        text = "Has error"
    SendCustomMessage(message=message,text=text)


def SendCustomMessage(message,text):
    bot.send_message(message.from_user.id,text)

# @bot.message_handler(content_types='text')
# def SendMessage(message):
#     # if message.
#     text ="SendMessage {}".format(message.from_user.first_name)
#     bot.send_message(message.from_user.id,text)


@bot.message_handler(commands=['addproduct'])
def handle_text(message):
    cid = message.chat.id
    msgPrice = bot.send_message(cid, 'Set your price:')
    bot.register_next_step_handler(msgPrice , step_Set_Price)

def step_Set_Price(message):

    print(message)
    cid = message.chat.id
    try:
        userPrice= float(message.text)
        text = "Hello {}".format(message.from_user.first_name)
        bot.send_message(message.from_user.id,text)
    except:
        msgPrice = bot.send_message(cid, 'Set your price:')
        bot.register_next_step_handler(msgPrice , step_Set_Price)




remove_user_dict = {}

# Handle '/start' and '/help'
@bot.message_handler(commands=['removeuser'])
def remove_user(message):
    msg = bot.reply_to(message, """Please select employee""")
    EmployeesButton(message,'Your employees')
    bot.register_next_step_handler(msg, process_username)
def process_username(message):
    try:
        chat_id = message.chat.id
        name = message.text

        text = "Dear {}, {}".format(message.from_user.first_name, 'sucessfully deleted')
        bot.reply_to(message, text)
    except Exception as e:
        bot.reply_to(message, 'oooops')





finish_task_dict = {}


def EmployeeActiveTasks(message,text):
    r = requests.get("{}".format('http://159.65.72.2/bot-api/employees/'))
    data = json.loads(r.text)
    key = ReplyKeyboardMarkup(True,True)
    # text = "Hello {}".format(message.from_user.first_name)
    print(data)
    for i in range(len(data['list'])):
        button = KeyboardButton(text=data['list'][i]['telegram_first_name'],request_contact=False)
        key.add(button)
    bot.send_message(message.from_user.id,text, reply_markup=key)
    pass

# Handle '/start' and '/help'
@bot.message_handler(commands=['removeuser'])
def finish_task(message):
    msg = bot.reply_to(message, """Please select task""")
    EmployeesButton(message,'Your employees')
    bot.register_next_step_handler(msg, process_username)

def process_username(message):
    try:
        chat_id = message.chat.id
        name = message.text

        text = "Dear {}, {}".format(message.from_user.first_name, 'sucessfully deleted')
        bot.reply_to(message, text)
    except Exception as e:
        bot.reply_to(message, 'oooops')




user_dict = {}

class User:
    def __init__(self, name):
        self.name = name
        self.age = None
        self.sex = None


# Handle '/start' and '/help'
@bot.message_handler(commands=['setuser'])
def ask_question(message):
    msg = bot.reply_to(message, """\
Hi there, I am Example bot.
What's your name?
""")
    bot.register_next_step_handler(msg, process_name_step)


def process_name_step(message):
    try:
        chat_id = message.chat.id
        name = message.text
        user = User(name)
        user_dict[chat_id] = user
        msg = bot.reply_to(message, 'How old are you?')
        user_dict['msg'] = msg
        bot.register_next_step_handler(msg, process_age_step)
    except Exception as e:
        bot.reply_to(message, 'oooops')


def process_age_step(message):
    try:
        chat_id = message.chat.id
        age = message.text
        if not age.isdigit():
            msg = bot.reply_to(message, 'Age should be a number. How old are you?')
            bot.register_next_step_handler(user_dict['msg'], process_age_step)
            return
        user = user_dict[chat_id]
        user.age = age
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
        markup.add('Male', 'Female')
        msg = bot.reply_to(message, 'What is your gender', reply_markup=markup)
        bot.register_next_step_handler(user_dict['msg'], process_sex_step)
    except Exception as e:
        bot.reply_to(message, 'oooops')


def process_sex_step(message):
    try:
        chat_id = message.chat.id
        sex = message.text
        user = user_dict[chat_id]
        if (sex == u'Male') or (sex == u'Female'):
            user.sex = sex
        else:
            raise Exception()
        bot.send_message(chat_id, 'Nice to meet you ' + user.name + '\n Age:' + str(user.age) + '\n Sex:' + user.sex)
    except Exception as e:
        bot.reply_to(message, 'oooops')


# Enable saving next step handlers to file "./.handlers-saves/step.save".
# Delay=2 means that after any change in next step handlers (e.g. calling register_next_step_handler())
# saving will hapen after delay 2 seconds.
bot.enable_save_next_step_handlers(delay=2)

# Load next_step_handlers from save file (default "./.handlers-saves/step.save")
# WARNING It will work only if enable_save_next_step_handlers was called!
bot.load_next_step_handlers()




bot.polling()