from django.conf.urls import url

from .views import *

app_name = 'bot-app'
urlpatterns = [
	url(r'^employees/$', GetList.as_view(), name='employees'),
	url(r'^get-start/(?P<m_id>[0-9]+)/(?P<username>[\w-]+)/$', GetStarted.as_view(), name='get-start'),
	url(r'^finished-task/(?P<id>[0-9]+)/(?P<username>[\w-]+)/$', FinishedTask.as_view(), name='finished-task'),
	url(r'^task-list/(?P<username>[\w-]+)/$', GetMyTasksList.as_view(), name='task-list'),
	url(r'^employees/$', GetList.as_view(), name='employees'),
	# url(r'^daily-check/(?P<m_id>[0-9]+)/$', daily_check, name='daily-check'),
	url(r'^daily-check/(?P<m_id>[0-9]+)/$', DailyCheckVIEW.as_view(), name='daily-check'),
]
