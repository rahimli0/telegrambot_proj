import datetime

from django.contrib.auth import get_user_model
from django.shortcuts import render
import telebot
# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from panel.models import DailyCheck, WorkDayUser, EmployeeTask
from panel.tasks import send_task_finished_info
from telegrambot_proj.settings import TELEGRAM_BOT_TOKEN
from django.utils.translation import ugettext as _

GUser = get_user_model()

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

class GetList(APIView):
    def get(self,request):
        data = {'list':[]}
        data['list'] = GUser.objects.filter().values('id', 'first_name')
        return Response(data)

    def post(self,request):
        pass

class GetStarted(APIView):
    def get(self,request,m_id,username):
        try:
            user = GUser.objects.get(username=username)
            user.m_id = m_id
            if request.POST.get('first_name', ''):
                user.first_name = request.POST.get('first_name', '')
            if request.POST.get('last_name', ''):
                user.last_name = request.POST.get('last_name', '')
            user.save()
            text = _('Welcomeback')
        except:
            text = _('You have no account in the system')
        data = {'message':  text}
        return Response(data)

class MyTasks(APIView):
    def get(self,request,username):
        data = {'list': [],'result_code':0}
        try:
            user = GUser.objects.get(username=username)
            tasks = EmployeeTask.objects.filter(employee=user,is_finished=False,is_canceled=False)

            data['list'] = tasks.values('id', 'first_name')
            data['result_code'] = 1
        except:
            pass
        return Response(data)


class FinishedTask(APIView):
    def get(self,request,id,username):
        data = {'result': '','result_code':0}
        try:
            task = EmployeeTask.objects.get(employee__username=username,id=id,is_canceled=False)
            if task.is_finished:
                data['result'] = 'You have already finished'
                data['result_code'] = 2
            else:
                data['result'] = 'Your request has added succesfully'
                data['result_code'] = 1
                task.is_finished = True
                task.save()
                send_task_finished_info(id )
        except:
            data['result'] = 'Has error happended'
        return Response(data)



class GetMyTasksList(APIView):
    def get(self,request,username):
        tasks = EmployeeTask.objects.filter(employee__username=username,is_finished=False,is_canceled=False)

        data = {'list': []}
        data['list'] = tasks.values('id', 'title')
        return Response(data)



# @api_view(['POST'])
# def daily_check(request,m_id):
#     try:
#         now = datetime.datetime.now()
#         user = GUser.objects.get(m_id=m_id,is_active=True)
#         try:
#             DailyCheck(user=user,is_verified=False,date_day=now.date(),date=now).save()
#             return Response({'result': 'Your request added'})
#         except:
#             return Response({'result': 'Already checking'})
#     except:
#         return Response({'result': 'User does not exist'})

class DailyCheckVIEW(APIView):
    def get(self,request,m_id):
        try:
            now = datetime.datetime.now()
            user = GUser.objects.get(m_id=m_id,is_active=True)

            try:
                user_workdays = WorkDayUser.objects.filter(user=user).values_list('work_day_id', flat=True)
                if now.weekday() in user_workdays :
                    DailyCheck(user=user,is_verified=False,date_day=now.date(),date=now).save()
                    res_text = 'Your request added successfully'
                else:
                    res_text = 'Today is not your work day'
                return Response({'result': res_text})
            except:
                return Response({'result': 'You are already checking today'})
        except:
            return Response({'result': 'You have not account in system'})

    def post(self,request):
        pass

