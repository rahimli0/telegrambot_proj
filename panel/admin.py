from django.contrib import admin

# Register your models here.
from panel.models import *

admin.site.register(DailyCheck)
admin.site.register(Section)
admin.site.register(UserRule)
admin.site.register(RuleUserModel)