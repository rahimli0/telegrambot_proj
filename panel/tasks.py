import json

import requests
import telebot
# Create your views here.
import telegram
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery
from telegram import ParseMode

from telegrambot_proj.settings import TELEGRAM_BOT_TOKEN

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

from telebot import types



def confirm_request_dailycheck():
    pass

def send_task_finished_info(t_id):
    from panel.models import EmployeeTask
    # try:
    data_obj = EmployeeTask.objects.get(id=t_id)

    text = "{}, {} has finished this task  \n ** {} ** \n {}".format(data_obj.author.first_name, data_obj.employee.first_name,data_obj.title,data_obj.content,)
    bot.send_message(int(data_obj.employee.m_id),text,parse_mode=ParseMode.MARKDOWN)
    # except:
    #     pass

def send_task_canceled_info(t_id):
    from panel.models import EmployeeTask
    # try:
    data_obj = EmployeeTask.objects.get(id=t_id)

    text = "{} This task  has cencelled \n ** {} ** \n {}".format(data_obj.employee.first_name,data_obj.title,data_obj.content,)
    bot.send_message(int(data_obj.employee.m_id),text,parse_mode=ParseMode.MARKDOWN)

def send_task_created_info(t_id):
    from panel.models import EmployeeTask
    try:
        data_obj = EmployeeTask.objects.get(id=t_id)
        text = "{}, This task has created  by ** {} ** \n ** {} ** \n {}".format(data_obj.employee.first_name, data_obj.author.first_name,data_obj.title,data_obj.content,)
        bot.send_message(int(data_obj.employee.m_id),text,parse_mode=ParseMode.MARKDOWN)
    except:
        pass