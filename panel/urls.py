from django.conf.urls import url

from .views import *

app_name = 'userprofile'
urlpatterns = [
	url(r'^$', index, name='index'),


	url(r'^user/list/$', user_list, name='user-list'),
	url(r'^user/list/removed/$', user_list_removed, name='user-list-removed'),
	url(r'^user/create/', user_create, name='user-create'),
	url(r'^user/edit/(?P<id>[0-9]+)/', user_edit, name='user-edit'),
	url(r'^user/remove/(?P<id>[0-9]+)/', user_remove, name='user-remove'),

	url(r'^section/list/$', section_list, name='section-list'),
	url(r'^section/create/', section_create, name='section-create'),
	url(r'^section/edit/(?P<id>[0-9]+)/', section_edit, name='section-edit'),
	url(r'^section/remove/(?P<id>[0-9]+)/', section_remove, name='section-remove'),

	url(r'^userrule/list/$', userrule_list, name='userrule-list'),
	url(r'^userrule/create/', userrule_create, name='userrule-create'),
	url(r'^userrule/edit/(?P<id>[0-9]+)/', userrule_edit, name='userrule-edit'),
	url(r'^userrule/remove/(?P<id>[0-9]+)/', userrule_remove, name='userrule-remove'),


	url(r'^workday/list/$', workday_list, name='workday-list'),
	url(r'^workday/create/', workday_create, name='workday-create'),
	url(r'^workday/edit/(?P<id>[0-9]+)/', workday_edit, name='workday-edit'),
	url(r'^workday/remove/(?P<id>[0-9]+)/', workday_remove, name='workday-remove'),



	url(r'^task/list/$', task_list, name='task-list'),
	url(r'^task/create/', task_create, name='task-create'),
	url(r'^task/edit/(?P<id>[0-9]+)/', task_edit, name='task-edit'),
	url(r'^task/remove/(?P<id>[0-9]+)/', task_remove, name='task-remove'),



	url(r'^test/$', test, name='test'),
	url(r'^main/settings/$', main_settings, name='main-settings'),

	url(r'^user/list/$', user_list, name='user-list'),
	url(r'^daily-check/list/$', daily_check_list, name='daily-check-list'),
	# path('start', StartView.as_command_view()),
	# path('author', AuthorCommandView.as_command_view()),
	# path('author_inverse', AuthorInverseListView.as_command_view()),
	# path('author_query', login_required(AuthorCommandQueryView.as_command_view())),
	# path(UnknownView.as_command_view()),
	# path(r'author_(?P<name>\w+)', AuthorName.as_command_view()),


]
