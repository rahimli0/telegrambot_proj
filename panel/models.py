from django.conf import settings
from django.db import models
from django.db.models import signals
from django.utils.translation import ugettext as _

# Create your models here.
from general.functions import path_and_rename
from panel.utils import utils
from panel.tasks import send_task_finished_info, send_task_canceled_info, send_task_created_info


class Section(models.Model):
    title = models.CharField(max_length=255)
    director = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True,related_name='director')
    short_text = models.CharField(max_length=500,blank=True,null=True)
    deleted = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title



class RuleUserModel(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    userrule = models.ForeignKey("UserRule",on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ['user', 'userrule']

class WorkDay(models.Model):
    title = models.CharField(max_length=255)
    day = utils.IntegerRangeField(min_value=1, max_value=7,unique=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

class WorkDayUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    work_day = models.ForeignKey("WorkDay",on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ['user', 'work_day']

class DailyCheck(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True)
    is_verified = models.BooleanField(default=False)
    date_day = models.DateField()
    date = models.DateTimeField()
    class Meta:
        unique_together = ['date_day', 'user']

class EmployeeTask(models.Model):
    title = models.CharField(max_length=255)
    section = models.ForeignKey('Section',blank=True,null=True,related_name='section_for')
    author = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True,related_name='author_for')
    employee = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True,related_name='employee_for')
    content = models.TextField(max_length=500,blank=True,null=True)
    is_finished = models.BooleanField(default=False)
    is_canceled = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

def employee_task_item_remove(sender, instance, *args, **kwargs):
    send_task_canceled_info(instance.id,)
def employee_task_item_save(sender, instance, created, *args, **kwargs):
    print('')
    # if instance.is_finished:
    #     send_task_finished_info(instance.id,)
    if instance.is_canceled:
        send_task_canceled_info(instance.id,)
    if created:
        send_task_created_info(instance.id,)
signals.post_save.connect(employee_task_item_save, sender=EmployeeTask)
signals.pre_delete.connect(employee_task_item_remove, sender=EmployeeTask)



UserRuleSTCHOICE  = (
    ('date',_('Date')),
    ('title',_('Title')),
    ('code',_('Code')),
    ('short_text',_('Short text')),
)

WorkdayCHOICE = (
    ('date',_('Date')),
    ('day',_('day')),
    ('title',_('Title')),
)
UserRuleCodeCHOICE = (
    ('dashboard',_('Dashboard')),
    ('main-settings',_('Main settings')),

    ('user-list',_('User list')),
    ('user-create',_('User create')),
    ('user-edit',_('User edit')),

    ('section-list',_('Section list')),
    ('section-create',_('Section create')),
    ('section-edit',_('Section edit')),
    ('section-remove',_('Section remove')),

    ('workday-list',_('Workday list')),
    ('workday-create',_('Workday create')),
    ('workday-edit',_('Workday edit')),
    ('workday-remove',_('Workday remove')),

    ('task-list',_('Task list')),
    ('task-create',_('Task create')),
    ('task-edit',_('Task edit')),
    ('task-remove',_('Task remove')),
)

class UserRule(models.Model):
    title = models.CharField(max_length=255)
    code = models.CharField(choices=UserRuleCodeCHOICE,max_length=50,unique=True)
    short_text = models.CharField(max_length=500,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

class MainSettings(models.Model):
    title = models.CharField(max_length=255)
    delay_days = models.IntegerField(default=0)
    big_logo = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    small_logo = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title