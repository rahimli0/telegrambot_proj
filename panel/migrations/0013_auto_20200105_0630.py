# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-05 06:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panel', '0012_section_deleted'),
    ]

    operations = [
        migrations.AddField(
            model_name='employeetask',
            name='is_canceled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='employeetask',
            name='is_finished',
            field=models.BooleanField(default=False),
        ),
    ]
