# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2020-01-05 12:15
from __future__ import unicode_literals

from django.db import migrations, models
import general.functions


class Migration(migrations.Migration):

    dependencies = [
        ('panel', '0013_auto_20200105_0630'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainsettings',
            name='big_logo',
            field=models.ImageField(blank=True, null=True, upload_to=general.functions.path_and_rename),
        ),
        migrations.AddField(
            model_name='mainsettings',
            name='small_logo',
            field=models.ImageField(blank=True, null=True, upload_to=general.functions.path_and_rename),
        ),
        migrations.AlterField(
            model_name='userrule',
            name='code',
            field=models.CharField(choices=[('dashboard', 'Dashboard'), ('main-settings', 'Main settings'), ('user-list', 'User list'), ('user-create', 'User create'), ('user-edit', 'User edit'), ('section-list', 'Section list'), ('section-create', 'Section create'), ('section-edit', 'Section edit'), ('section-remove', 'Section remove'), ('workday-list', 'Workday list'), ('workday-create', 'Workday create'), ('workday-edit', 'Workday edit'), ('workday-remove', 'Workday remove'), ('task-list', 'Task list'), ('task-create', 'Task create'), ('task-edit', 'Task edit'), ('task-remove', 'Task remove')], max_length=50, unique=True),
        ),
    ]
