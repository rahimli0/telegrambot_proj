import hashlib
import random

from celery.utils.time import timezone
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from datetime import datetime
from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import ugettext as _

from base_user.tools.common import USERTYPES, AdminstrativUSERTYPES
from panel.decorators import check_telegram_user, is_admin, is_adminstrativ

GUser = get_user_model()

from django_telegram_login.widgets.constants import (
    SMALL,
    MEDIUM,
    LARGE,
    DISABLE_USER_PHOTO,
)
from django_telegram_login.widgets.generator import (
    create_callback_login_widget,
    create_redirect_login_widget,
)
from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)

from general.views import base_auth
from panel.forms import SectionModelForm, MainSearchForm, UserRuleModelForm, GeneralSearchForm, UserForm, \
    UserEditForm, MainSettingsForm, WorkDayModelForm, EmployeeTaskModelForm, TaskSearchForm, DailyCheckSearchForm
from panel.models import *

bot_name = settings.TELEGRAM_BOT_NAME
bot_token = settings.TELEGRAM_BOT_TOKEN
redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)

@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='dashboard')
def index(request):

    context = base_auth(req=request)
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        result_data = ''
        try:
            result_data = '{}'.format(render_to_string(
                "panel/include/home/main.html",
                {
                    'section_count': Section.objects.filter(deleted=False).count(),
                    'active_tasks_count': EmployeeTask.objects.filter(is_canceled=False,is_finished=False).count(),
                    'employees_count': GUser.objects.filter(usertype=2,is_deleted=False,is_active=True).count(),
                }
            ))
            message_code = 1
        except:
            pass
        data = {
            'main_result': result_data,
            'message_code': message_code,
        }
        return JsonResponse(data=data)
    return render(request, 'panel/dashboard.html', context=context)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def test(request):

    context = base_auth(req=request)
    return render(request, 'base-main.html', context=context)






USERSearchCHOICE = (
    ('first_name',_('First name')),
    ('last_name',_('Last name')),
    ('username',_('Username')),
    ('date',_('Date')),
)

DailyCheckCHOICE = (
    ('user__first_name',_('First name')),
    ('user__last_name',_('Last name')),
    ('date',_('Date')),
)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='user-list')
def user_list(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(USERSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            username = cleaned_data.get('username',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            if user.usertype == 1:
                data_list = GUser.objects.filter(is_deleted=False,is_active=True).order_by('{}{}'.format(order_type,field_list))
            else:
                data_list = GUser.objects.filter(is_deleted=False,is_active=True,usertype=2).order_by('{}{}'.format(order_type,field_list))
            if first_name:
                data_list = data_list.filter(first_name__icontains=first_name)

            if last_name:
                data_list = data_list.filter(last_name__icontains=last_name)

            if username:
                data_list = data_list.filter(username__icontains=username)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/users/include/list-item.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/users/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/users/user-list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='user-list-removed')
def user_list_removed(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(USERSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            username = cleaned_data.get('username',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            if user.usertype == 1:
                data_list = GUser.objects.filter(is_deleted=True).order_by('{}{}'.format(order_type,field_list))
            else:
                data_list = GUser.objects.filter(is_deleted=True,usertype=2).order_by('{}{}'.format(order_type,field_list))
            if first_name:
                data_list = data_list.filter(first_name__icontains=first_name)

            if last_name:
                data_list = data_list.filter(last_name__icontains=last_name)

            if username:
                data_list = data_list.filter(username__icontains=username)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/users/include/list-item-removed.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/users/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form
    context['extra_title'] = 'The removed'


    return render(request, 'panel/users/user-list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='user-list-removed')
def daily_check_list(request):
    import datetime
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'date',
    }
    search_form = DailyCheckSearchForm(DailyCheckCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            username = cleaned_data.get('username',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            first_date = cleaned_data.get('first_date',None)
            end_date = cleaned_data.get('end_date',None)

            if user.usertype == 1:
                data_list = DailyCheck.objects.filter(user__is_deleted=False).order_by('{}{}'.format(order_type,field_list))
            else:
                data_list = DailyCheck.objects.filter(user__is_deleted=False,usertype=2).order_by('{}{}'.format(order_type,field_list))
            if first_name:
                data_list = data_list.filter(user__first_name__icontains=first_name)

            if last_name:
                data_list = data_list.filter(user__last_name__icontains=last_name)

            if username:
                data_list = data_list.filter(user__username__icontains=username)

            if first_date:
                start_date_min = datetime.datetime.combine(first_date, datetime.time.min)
                data_list = data_list.filter(date__gte=start_date_min)

            if end_date:
                end_date_max = datetime.datetime.combine(end_date, datetime.time.max)
                data_list = data_list.filter(date__lte=end_date_max)
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/daily-check/include/list-item.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/daily-check/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/daily-check/list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='user-create')
def user_create(request):
    now = datetime.now()
    user = request.user
    context = base_auth(req=request)
    if user.usertype == 1 :
        usertype_choice = USERTYPES
    else:
        usertype_choice = AdminstrativUSERTYPES
    user_form = UserForm(usertype_choice,request.POST or None, request.FILES or None)
    if request.method == "POST" and request.is_ajax():
        message_code = 0
        if user_form.is_valid():
            clean_data = user_form.cleaned_data
            section_id = clean_data.get('section')
            rules = clean_data.get('rules')
            workdays = clean_data.get('workdays')
            # name = clean_data.get('name')
            # surname = clean_data.get('surname')
            email = clean_data.get('email')
            username = clean_data.get('username')
            phone = clean_data.get('phone')
            usertype = clean_data.get('usertype')
            password = clean_data.get('password')
            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(str(password).strip(), salt=salt)

            user_obj = GUser(
                             # first_name=name,
                             # last_name=surname,
                             email=email,
                             username=username,
                             phone=phone,
                             password=password,
                             usertype=usertype,
                             is_active=True,

             )
            if section_id:
                user_obj.section_id = section_id
            user_obj.save()
            if user_obj.usertype in [4]:
                if rules:
                    for rules_item in list(rules):
                        RuleUserModel.objects.create(user=user_obj,userrule_id=rules_item)
            if user_obj.usertype != 1:
                if workdays:
                    for workdays_item in list(workdays):
                        WorkDayUser.objects.create(user=user_obj,work_day_id=workdays_item)
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(user_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    context['form'] = user_form
    response = render(request, 'panel/users/user-form.html', context=context)
    return response



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='user-edit')
def user_edit(request,id):
    user = request.user
    context = base_auth(req=request)
    if user.usertype == 1:
        user_obj = get_object_or_404(GUser,id=id)
        usertype_choice = USERTYPES
    else:
        user_obj = get_object_or_404(GUser,id=id,usertype=2)
        usertype_choice = AdminstrativUSERTYPES
    inital_data = {
        'name':user_obj.first_name,
        'section':user_obj.section_id,
        'surname':user_obj.last_name,
        'email':user_obj.email,
        'username':user_obj.username,
        'usertype':user_obj.usertype,
        'phone':user_obj.phone,
        'rules':RuleUserModel.objects.filter(user=user_obj).values_list('userrule_id',flat=True),
        'workdays':WorkDayUser.objects.filter(user=user_obj).values_list('work_day_id',flat=True),
    }
    user_form = UserEditForm(id,usertype_choice,request.POST or None, request.FILES or None,initial=inital_data)


    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if user_form.is_valid():
            clean_data = user_form.cleaned_data
            section_id = clean_data.get('section',None)
            rules = clean_data.get('rules',None)
            workdays = clean_data.get('workdays',None)
            # name = clean_data.get('name',None)
            # surname = clean_data.get('surname',None)
            email = clean_data.get('email',None)
            username = clean_data.get('username',None)
            phone = clean_data.get('phone',None)
            usertype = clean_data.get('usertype',None)
            password = clean_data.get('password')

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            # user_obj.first_name=name
            # user_obj.last_name=surname
            user_obj.email=email
            user_obj.username=username
            user_obj.usertype=usertype
            user_obj.phone=phone
            if section_id:
                user_obj.section_id = section_id
            if password:
                password = make_password(password, salt=salt)
                user_obj.password=password
            user_obj.save()

            if user_obj.usertype == '4':
                if rules:
                    RuleUserModel.objects.filter(user=user_obj).exclude(userrule_id__in=list(rules)).delete()
                    for rules_item in list(rules):
                        obj, created = RuleUserModel.objects.get_or_create(
                            user=user_obj,
                            userrule_id=rules_item
                        )

            if user_obj.usertype != 1:
                if workdays:
                    WorkDayUser.objects.filter(user=user_obj).exclude(work_day_id__in=list(workdays)).delete()
                    for workdays_item in list(workdays):
                        WorkDayUser.objects.get_or_create(
                            user=user_obj,
                            work_day_id=workdays_item
                        )


            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully'),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(user_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)
            # user_form = UserEditForm(user_id=user_obj.id)
        # else:
        #     return HttpResponse(user_form.errors)

    context['form'] = user_form
    context['data_obj'] = user_obj
    response = render(request, 'panel/users/user-form.html', context=context)
    return response






@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='section-remove')
def user_remove(request,id):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        try:
            if user.usertype == 1:
                data_obj = get_object_or_404(GUser,id=id)
            else:
                data_obj = get_object_or_404(GUser,id=id,usertype=2)
            if data_obj.is_deleted:
                data_obj.is_active = True
                data_obj.is_deleted = False
            else:
                data_obj.is_active = False
                data_obj.is_deleted = True
            data_obj.save()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='section-list')
def section_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = MainSearchForm(request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = Section.objects.filter(deleted=False).order_by('{}{}'.format(order_type,field_list))
    #         gender = cleaned_data.get('gender',None)
    #         show_photo = cleaned_data.get('show_photo',None)
    #         _list_item_html = ''
            if search_text:
                data_list = data_list.filter(Q(title__icontains=search_text) | Q(short_text__icontains=search_text))
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            _list_item_html = ''
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            if data_list:
                for data_item in data_list:
                    # for_i+=1
                    _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                        "panel/section/include/section-list-item.html",
                    {
                        "data_item":data_item,
                        'base_profile': request.user,
                    }))

            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/section/include/main.html',
                        {
                            'list_html': _list_item_html,
                        }
                    )
                )
            else:
                main_html = _list_item_html
            data = {
                'main_result':"{}".format(main_html),
                # 'data_count':data_count,
            }
            return JsonResponse(data=data)
    # else:
    #     log_generation.delay(title=title, user_id=request.user.id, object_data='Customer',
    #                          operation='show', content='',
    #                          url=request.build_absolute_uri())
    context['search_form'] = search_form
    # context['title'] = title
    # context['type'] = type


    return render(request, 'panel/section/section-list.html', context=context)




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='section-create')
def section_create(request):
    context = base_auth(req=request)
    user = request.user
    form = SectionModelForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            form = SectionModelForm()
        # else:
        #     return HttpResponse(user_form.errors)
    context['form'] = form

    return render(request, 'panel/section/section-form.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='section-edit')
def section_edit(request,id):
    user = request.user

    data_obj = get_object_or_404(Section,id=id,deleted=False)
    context = base_auth(req=request)
    if request.method == "POST":
        form = SectionModelForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            post.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
    else:
        form = SectionModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/section/section-form.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='section-remove')
def section_remove(request,id):
    if request.method == 'POST' and request.is_ajax():
        try:
            data_obj = get_object_or_404(Section,id=id)
            data_obj.deleted = True
            data_obj.save()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404

# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def userrule_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(UserRuleSTCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = UserRule.objects.filter().order_by('{}{}'.format(order_type,field_list))
            if search_text:
                data_list = data_list.filter(Q(title__icontains=search_text) | Q(short_text__icontains=search_text))
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            _list_item_html = ''
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            if data_list:
                for data_item in data_list:
                    # for_i+=1
                    _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                        "panel/rules/include/list-item.html",
                    {
                        "data_item":data_item,
                        'base_profile': request.user,
                    }))

            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/rules/include/main.html',
                        {
                            'list_html': _list_item_html,
                        }
                    )
                )
            else:
                main_html = _list_item_html
            data = {
                'main_result':"{}".format(main_html),
                # 'data_count':data_count,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/rules/list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def userrule_create(request):
    context = base_auth(req=request)
    user = request.user
    form = UserRuleModelForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            form = UserRuleModelForm()
        # else:
        #     return HttpResponse(user_form.errors)
    context['form'] = form

    return render(request, 'panel/rules/form.html', context=context)




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def userrule_edit(request,id):
    user = request.user

    data_obj = get_object_or_404(UserRule,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = UserRuleModelForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            post.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
    else:
        form = UserRuleModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/rules/form.html', context=context)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def userrule_remove(request,id):
    if request.method == 'POST' and request.is_ajax():
        try:
            data_obj = get_object_or_404(UserRule,id=id)
            data_obj.delete()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404

# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='main-settings')
def main_settings(request):
    context = base_auth(req=request)
    user = request.user
    try:
        data_obj = MainSettings.objects.filter().order_by('-date').get()
        context = base_auth(req=request)
        if request.method == "POST":
            form = MainSettingsForm(request.POST or None,request.FILES or None, instance=data_obj)
            if form.is_valid():
                post = form.save(commit=False)
                post.title = 'Main'
                post.save()
                messages.add_message(request, messages.SUCCESS, 'Succesfully changed')

        else:
            form = MainSettingsForm(instance=data_obj)
        context['form'] = form
        context['data_obj'] = data_obj
    except:
        form = MainSettingsForm(request.POST or None,request.FILES or None)
        if request.method == 'POST':
            if form.is_valid():
                form_val = form.save(commit=False)
                form_val.save()
                messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
                form = MainSettingsForm()
            # else:
            #     return HttpResponse(employee_form.errors)
        context['form'] = form

    return render(request, 'panel/general/settings/edit.html', context=context)




# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='workday-list')
def workday_list(request):

    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(WorkdayCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)
            data_list = WorkDay.objects.filter().order_by('{}{}'.format(order_type,field_list))
            if search_text:
                data_list = data_list.filter(Q(title__icontains=search_text))
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            _list_item_html = ''
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            if data_list:
                for data_item in data_list:
                    _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                        "panel/general/workdays/include/list-item.html",
                    {
                        "data_item":data_item,
                        'base_profile': request.user,
                    }))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/general/workdays/include/main.html',
                        {
                            'list_html': _list_item_html,
                        }
                    )
                )
            else:
                main_html = _list_item_html
            data = {
                'main_result':"{}".format(main_html),
                # 'data_count':data_count,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/general/workdays/list.html', context=context)




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='workday-create')
def workday_create(request):
    context = base_auth(req=request)
    user = request.user
    form = WorkDayModelForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            form = WorkDayModelForm()
        # else:
        #     return HttpResponse(user_form.errors)
    context['form'] = form

    return render(request, 'panel/general/workdays/form.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='workday-edit')
def workday_edit(request,id):
    user = request.user

    data_obj = get_object_or_404(WorkDay,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = WorkDayModelForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            post.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
    else:
        form = WorkDayModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/general/workdays/form.html', context=context)




@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='workday-remove')
def workday_remove(request,id):
    if request.method == 'POST' and request.is_ajax():
        try:
            data_obj = get_object_or_404(WorkDay,id=id)
            data_obj.delete()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404

# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------

TaskCHOICE = (
    ('date',_('Date')),
)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='task-list')
def task_list(request):
    import datetime
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'date',
    }
    search_form = TaskSearchForm(TaskCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            search_text = cleaned_data.get('search',None)
            first_date = cleaned_data.get('first_date',None)
            end_date = cleaned_data.get('end_date',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            data_list = EmployeeTask.objects.filter(author=request.user).order_by('{}{}'.format(order_type,field_list))
            if search_text:
                data_list = data_list.filter(Q(content__icontains=search_text) | Q(employee__section__title__icontains=search_text))
            if first_date:
                start_date_min = datetime.datetime.combine(first_date, datetime.time.min)
                data_list = data_list.filter(date__gte=start_date_min)

            if end_date:
                end_date_max = datetime.datetime.combine(end_date, datetime.time.max)
                data_list = data_list.filter(date__lte=end_date_max)
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            _list_item_html = ''
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            if data_list:
                for data_item in data_list:
                    _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                        "panel/tasks/include/list-item.html",
                    {
                        "data_item":data_item,
                        'base_profile': request.user,
                    }))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/tasks/include/main.html',
                        {
                            'list_html': _list_item_html,
                        }
                    )
                )
            else:
                main_html = _list_item_html
            data = {
                'main_result':"{}".format(main_html),
                # 'data_count':data_count,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/tasks/list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='task-create')
def task_create(request):
    context = base_auth(req=request)
    user = request.user
    sections_ids = Section.objects.filter(director=request.user).values_list('id',flat=True)
    form = EmployeeTaskModelForm(sections_ids,request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.author = request.user
            form_val.save()
            form_val.section = form_val.employee.section
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            form = EmployeeTaskModelForm(sections_ids=sections_ids)
        # else:
        #     return HttpResponse(user_form.errors)
    context['form'] = form

    return render(request, 'panel/tasks/form.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='task-edit')
def task_edit(request,id):
    user = request.user

    data_obj = get_object_or_404(EmployeeTask,id=id,author=request.user)
    context = base_auth(req=request)
    sections_ids = Section.objects.filter(director=request.user).values_list('id',flat=True)
    if request.method == "POST":
        form = EmployeeTaskModelForm(request.POST,sections_ids= sections_ids,instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            post.save()
            post.section = post.employee.section
            post.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
    else:
        form = EmployeeTaskModelForm(sections_ids= sections_ids,instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/tasks/form.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_adminstrativ(rule_item='task-remove')
def task_remove(request,id):
    if request.method == 'POST' and request.is_ajax():
        # try:
        data_obj = get_object_or_404(EmployeeTask,id=id,author=request.user)
        data_obj.delete()
        message_code = 1
        # except:
        #     message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404



