from django.conf.urls import url

from .views import *

app_name = 'base-user'
urlpatterns = [
	url(r'^callback/$', callback, name='callback'),
	url(r'^redirect/$', redirect, name='redirect'),
	url(r'^logout/$', log_out, name='logout'),
]
